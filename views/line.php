<?php defined('SYSPATH') OR die('No direct script access!');?>
<?php if($breadcrumbs->count()):?>
<div class="breadcrumbs<?php echo !empty($addon_class) ? ' '.$addon_class : ''?>">
<?php $i = 0; foreach($breadcrumbs as $breadcrumb):?>
<?php if($i > 0):?>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?endif?>
<?php if($set_urls && ! empty($breadcrumb->url)):?>
<a href="<?=$breadcrumb->url?>"<?php if($breadcrumb->active):?><?= ' class="'.$active_class.'"'?><?endif?>><?=$breadcrumb->name?></a>
<?else:?>
<span<?php if($breadcrumb->active):?><?= ' class="'.$active_class.'"'?><?endif?>><?=$breadcrumb->name?></span>
<?endif?>
<?php $i++; endforeach; ?>
</div>
<?endif?>