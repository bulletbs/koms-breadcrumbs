<?php defined('SYSPATH') OR die('No direct script access!'); 

return array(
	'default' => array(
		'view' => 'line',
		'urls' => true, // change to false if you don't want urls on the breadcrumb items
		'active_class' => 'active',
		'addon_class' => '',
	)
);